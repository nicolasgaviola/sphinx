.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a GitLab Pages con documentación Sphinx's
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices y tablas
==================

* :ref:`genindex`
* :ref:`search`

Fork this project
==================

* https://gitlab.com/pages/sphinx
